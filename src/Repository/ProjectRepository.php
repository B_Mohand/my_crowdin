<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function createproject($user, $em, $data, $slugname, $sourceLanguage, $creationDate)
    {
        $project = new Project;
        $project->setProjectName($data['name']);
        $project->setSlugName($slugname);
        $project->setCreationDate($creationDate);
        $project->setSourceLanguage($sourceLanguage);
        $project->setMembers([$user]);
        $project->setOwner($user);

        $em->persist($project);
        $em->flush();
    }

    public function addLanguage($project, $em, $languages)
    {
        $project->setTargetLanguage($languages);

        $em->persist($project);
        $em->flush();
    }

    public function deleteProject($project, $em)
    {
        $em->remove($project);
        $em->flush();
    }
    // /**
    //  * @return Project[] Returns an array of Project objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Project
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
