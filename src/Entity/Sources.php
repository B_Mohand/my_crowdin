<?php

namespace App\Entity;

use App\Repository\SourcesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SourcesRepository::class)
 */
class Sources
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language;

    /**
     * @ORM\Column(type="integer")
     */
    private $projectID;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $translatedVersion = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getProjectID(): ?int
    {
        return $this->projectID;
    }

    public function setProjectID(int $projectID): self
    {
        $this->projectID = $projectID;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getTranslatedVersion(): ?array
    {
        return $this->translatedVersion;
    }

    public function setTranslatedVersion(?array $translatedVersion): self
    {
        $this->translatedVersion = $translatedVersion;

        return $this;
    }
}
