<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProjectRepository;
use App\Repository\SourcesRepository;
use App\Repository\TranslationsRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Intl\Languages;

class FrontendController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function home()
    {
        return $this->redirectToRoute('app_myprofile');
    }

    /**
     * @Route("/profile", name="app_myprofile")
     */
    public function myProfile(ProjectRepository $projectsRepo)
    {
    	if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $user = $this->getUser();
        $projects = $projectsRepo->findBy(['owner' => $user->getPseudo()]);

        return $this->render('frontend/profile.html.twig', [
            'user' => $user,
            'projects' => $projects
        ]);
    }

    /**
     * @Route("/explore", name="app_explore")
     */
    public function explore()
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        return $this->render('frontend/explore.html.twig');
    }

    /**
     * @Route("/randomProject", name="app_random_project")
     */
    public function randomProject(ProjectRepository $projectsRepo)
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $user = $this->getUser();
        $allProjects = $projectsRepo->findAll();
        $random_keys = array_rand($allProjects, 1);

        $randomProject = $allProjects[$random_keys];

        return $this->redirectToRoute('app_project', ['slugname' => $randomProject->getSlugname()]);
    }

    /**
     * @Route("/profile/{pseudo}", name="app_profile")
     */
    public function profil(string $pseudo, UserRepository $userRepo, ProjectRepository $projectsRepo)
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $user = $userRepo->findOneBy(['pseudo' => $pseudo]);
        $projects = $projectsRepo->findBy(['owner' => $user->getPseudo()]);

        return $this->render('frontend/profile.html.twig', [
            'user' => $user,
            'projects' => $projects
        ]);
    }

    /**
     * @Route("/project/{slugname}", name="app_project")
     */
    public function project(string $slugname, ProjectRepository $projectsRepo, UserRepository $userRepo, SourcesRepository $sourcesRepo, TranslationsRepository $translationsRepo)
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $project = $projectsRepo->findOneBy(['slugname' => $slugname]);
        $owner = $userRepo->findOneBy(['pseudo' => $project->getOwner()]);
        $sources = $sourcesRepo->findBy(['projectID' => $project->getId()]);
        $translations = $translationsRepo->findBy(['idProject' => $project->getId()]);

        return $this->render('frontend/project.html.twig', [
            'project' => $project,
            'owner' => $owner,
            'sources' => $sources,
            'translations' => $translations
        ]);
    }

    /**
     * @Route("/project/translate/{slugname}/{language}", name="app_project_translate")
     */
    public function projectTranslate(string $slugname, string $language, ProjectRepository $projectsRepo, SourcesRepository $sourcesRepo)
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $project = $projectsRepo->findOneBy(['slugname' => $slugname]);
        $sources = $sourcesRepo->findBy(['projectID' => $project->getID()]);

        return $this->render('frontend/projectTranslate.html.twig', [
            'project' => $project,
            'sources' => $sources,
            'language' => $language
        ]);
    }

    /**
     * @Route("/translate/sources/{slugname}/{idSource}/{language}", name="app_source_translate")
     */
    public function sourceTranslate(string $slugname, int $idSource, string $language, Request $request, EntityManagerInterface $em, SourcesRepository $sourcesRepo, ProjectRepository $projectsRepo, TranslationsRepository $translationsRepo)
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $project = $projectsRepo->findOneBy(['slugname' => $slugname]);
        $source = $sourcesRepo->find($idSource);

        $translateForm = $this->createFormBuilder()
            ->add('translation', TextareaType::class, [
                'label' => "Traduction :",
                'attr' => [
                    'placeholder' => "Traduire ici"
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Sauvegarder",
                'attr' => [
                    'class' => 'btn btn-primary'
                ] 
            ])
            ->getForm()
        ;

        $translateForm->handleRequest($request);
        if ($translateForm->isSubmitted() && $translateForm->isValid())
        {
            $data = $translateForm->getData();

            $translatedVersions = $source->getTranslatedVersion();
            if ($translatedVersions == null)
                $translatedVersions = [];

            if (!in_array($language, $translatedVersions)){
                array_push($translatedVersions, $language);
                $sourcesRepo->addTranslatedVersions($em, $source, $translatedVersions);
                $translationsRepo->addTranslation($em, $data['translation'], $idSource, $project->getId(), $language);
            }
            return $this->redirectToRoute('app_project_translate', ['slugname' => $slugname, 'language' => $language]);
        }

        return $this->render('frontend/sourceTranslate.html.twig', [
            'source' => $source,
            'project' => $project,
            'language' => $language,
            'translateForm' => $translateForm->createView()
        ]);
    }

    /**
     * @Route("/project/{slugname}/settings", name="app_project_settings")
     */
    public function projectSettings(string $slugname, ProjectRepository $projectsRepo, UserRepository $userRepo, Request $request, EntityManagerInterface $em, SluggerInterface $slugger, SourcesRepository $sourcesRepo)
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $project = $projectsRepo->findOneBy(['slugname' => $slugname]);
        $owner = $userRepo->findOneBy(['pseudo' => $project->getOwner()]);

        $languagesForm = $this->createFormBuilder()
            ->add('languages', LanguageType::class, [
                'label' => "Ajouter une langue cible"
            ])
            ->add('Ajouter', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary mt-3']
            ])
            ->getForm()
        ;

        $languages = $project->getTargetLanguage();

        $languagesForm->handleRequest($request);
        if ($languagesForm->isSubmitted() && $languagesForm->isValid())
        {
            $data = $languagesForm->getData();
            $newLanguage = Languages::getName($data['languages']);

            if (!in_array($newLanguage, $languages)){
                if ($newLanguage != $project->getSourceLanguage())
                array_push($languages, $newLanguage);
                $projectsRepo->addLanguage($project, $em, $languages);
            }
            return $this->redirectToRoute('app_project_settings', ['slugname' => $project->getSlugname()]);
        }

        $addFilesForm = $this->createFormBuilder()
            ->add('file', FileType::class, array(
                'label' => 'CSV',
                'constraints' => [
                    new File([
                        'maxSize' => '8M',
                        'mimeTypes' => ['text/csv', 'text/plain'],
                        'mimeTypesMessage' => 'Le fichier transmis n\'es pas au bon format',
                    ])
                ],
            ))
            ->add('create', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Ajouter les sources via un CSV'
            ])
            ->getForm()
        ;

        $addFilesForm->handleRequest($request);
        if ($addFilesForm->isSubmitted() && $addFilesForm->isValid()) {
            $data = $addFilesForm->getData();
            $originalFilename = pathinfo($data['file']->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $fileName = $safeFilename.'-'.uniqid().'.'.$data['file']->guessExtension();

            $data['file']->move(
                $this->getParameter('filesToTranslate'),
                $fileName
            );

            $file = fopen($this->getParameter('filesToTranslate') . "/" . $fileName, "r");

            $column = fgetcsv($file, 10000, ";");

            for ($i = 2; isset($column[$i]); $i++)
                $sourcesRepo->addSource($em, $column[$i], $project->getSourceLanguage(), $project->getId());
            return $this->redirectToRoute('app_project_settings', ['slugname' => $project->getSlugname()]);
        }

        $addSourceForm = $this->createFormBuilder()
            ->add('content', TextType::class, array(
                'label' => "Source"
            ))
            ->add('submit', SubmitType::class, array(
                'label' => "Ajouter la source",
                'attr' => ['class' => 'btn btn-primary'] 
            ))
            ->getForm()
        ;

        $addSourceForm->handleRequest($request);
        if ($addSourceForm->isSubmitted() && $addSourceForm->isValid()) {
            $data = $addSourceForm->getData();
            $sourcesRepo->addSource($em, $data['content'], $project->getSourceLanguage(), $project->getId());
            return $this->redirectToRoute('app_project_settings', ['slugname' => $project->getSlugname()]);
        }

        return $this->render('frontend/projectSettings.html.twig', [
            'project' => $project,
            'owner' => $owner,
            'languagesForm' => $languagesForm->createView(),
            'languages' => $languages,
            'addFilesForm' => $addFilesForm->createView(),
            'addSourceForm' => $addSourceForm->createView() 
        ]);
    }

    /**
     * @Route("/createproject", name="app_createproject")
     */
    public function createproject(EntityManagerInterface $em, Request $request, ProjectRepository $projectsRepo, SluggerInterface $slugger)
    {
        if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $user = $this->getUser();

        $pseudoUser = $user->getPseudo();

        $createprojectForm = $this->createFormBuilder()
            ->add('name', TextType::class, [
                'label' => "Nom du projet",
                'attr' => [
                    'placeholder' => "Généralement, le nom de votre site Web ou application"
                ]
            ])
            ->add('originLanguage', LanguageType::class, [
                'label' => "Langue originelle"
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Créer un projet",
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm()
        ;

        $createprojectForm->handleRequest($request);
        if ($createprojectForm->isSubmitted() && $createprojectForm->isValid())
        {
            $data = $createprojectForm->getData();
            $sourceLanguage = Languages::getName($data['originLanguage']);
            $slugname = strtolower($slugger->slug($data['name']));

            $creationDate = new \DateTime();
            $projectsRepo->createproject($pseudoUser, $em, $data, $slugname, $sourceLanguage, $creationDate);

            return $this->redirectToRoute('app_myprofile');
        }

        return $this->render('frontend/createproject.html.twig', [
            'user' => $user,
            'createprojectForm' => $createprojectForm->createView()
        ]);
    }

    /**
     * @Route("/settings", name="app_settings")
     */
    public function settings(UserRepository $userRepo, Request $request, EntityManagerInterface $em, SluggerInterface $slugger)
    {
    	if (!($this->getUser())) {
             return $this->redirectToRoute('app_login');
        }
        $user = $this->getUser();

        $imageForm = $this->createFormBuilder()
            ->add('avatar', FileType::class, array(
                'constraints' => [
                    new File([
                        'maxSize' => '8M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Veuillez choisir une image valide',
                    ])
                ],
            ))
            ->add('Sauvegarder', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary mt-3']
            ])
            ->getForm()
        ;

        if ($user->getFullName())
            $full_name = $user->getFullName();
        else
            $full_name = null;
        if ($user->getGender())
            $gender = $user->getGender();
        else
            $gender = null;
        if ($user->getDescription())
            $description = $user->getDescription();
        else
            $description = null;

        $profileForm = $this->createFormBuilder()
            ->add('full_name', TextType::class, [
                'label' => "Nom complet",
                'required' => false,
                'attr' => [
                    'placeholder' => "Par exempe John Doe",
                    'value' => $full_name
                ]
            ])
            ->add('user_name', TextType::class, [
                'label' => "Nom d'utilisateur",
                'attr' => [
                    'value' => $user->getPseudo()
                ]
            ])
            ->add('email', TextType::class, [
                'attr' => [
                    'value' => $user->getEmail()
                ]
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Homme' => 'male',
                    'Femme' => 'female'
                ],
                'attr' => [
                    'value' => $gender
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => "À propos de moi",
                'required' => false,
                'data' => $description
            ])
            ->add('Sauvegarder', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary mt-3']
            ])
            ->getForm()
        ;

        $languagesForm = $this->createFormBuilder()
            ->add('languages', LanguageType::class, [
                'label' => "Ajouter une langue"
            ])
            ->add('Ajouter', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary mt-3']
            ])
            ->getForm()
        ;

        $imageForm->handleRequest($request);
        if ($imageForm->isSubmitted() && $imageForm->isValid())
        {
            $data = $imageForm->getData();
            $originalFilename = pathinfo($data['avatar']->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$data['avatar']->guessExtension();
            $data['avatar']->move(
                $this->getParameter('avatars_directory'),
                $newFilename
            );

            $userRepo->updateProfilePicture($user, $em, $newFilename);

            return $this->redirectToRoute('app_settings');
        }

        $profileForm->handleRequest($request);
        if ($profileForm->isSubmitted() && $profileForm->isValid())
        {
            $data = $profileForm->getData();
            $userRepo->updateGeneralInformations($user, $em, $data);
        }

        $languages = $user->getLanguages();

        $languagesForm->handleRequest($request);
        if ($languagesForm->isSubmitted() && $languagesForm->isValid())
        {
            $data = $languagesForm->getData();
            $newLanguage = Languages::getName($data['languages']);

            if (!in_array($newLanguage, $languages)){
                array_push($languages, $newLanguage);
                $userRepo->addLanguage($user, $em, $languages);
            }

            return $this->redirectToRoute('app_settings');
        }

        return $this->render('frontend/settings.html.twig', [
        	'user' => $user,
            'languages' => $languages,
        	'imageForm' => $imageForm->createView(),
            'profileForm' => $profileForm->createView(),
            'languagesForm' => $languagesForm->createView()
        ]);
    }
}
