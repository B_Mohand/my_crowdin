<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerTV1bsQk\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerTV1bsQk/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerTV1bsQk.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerTV1bsQk\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerTV1bsQk\App_KernelDevDebugContainer([
    'container.build_hash' => 'TV1bsQk',
    'container.build_id' => 'b705a892',
    'container.build_time' => 1593102314,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerTV1bsQk');
