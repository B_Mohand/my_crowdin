<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_home', '_controller' => 'App\\Controller\\FrontendController::home'], null, null, null, false, false, null]],
        '/profile' => [[['_route' => 'app_myprofile', '_controller' => 'App\\Controller\\FrontendController::myProfile'], null, null, null, false, false, null]],
        '/explore' => [[['_route' => 'app_explore', '_controller' => 'App\\Controller\\FrontendController::explore'], null, null, null, false, false, null]],
        '/randomProject' => [[['_route' => 'app_random_project', '_controller' => 'App\\Controller\\FrontendController::randomProject'], null, null, null, false, false, null]],
        '/createproject' => [[['_route' => 'app_createproject', '_controller' => 'App\\Controller\\FrontendController::createproject'], null, null, null, false, false, null]],
        '/settings' => [[['_route' => 'app_settings', '_controller' => 'App\\Controller\\FrontendController::settings'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/verify/email' => [[['_route' => 'app_verify_email', '_controller' => 'App\\Controller\\RegistrationController::verifyUserEmail'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/pro(?'
                    .'|file/([^/]++)(*:189)'
                    .'|ject/(?'
                        .'|([^/]++)(*:213)'
                        .'|translate/([^/]++)/([^/]++)(*:248)'
                        .'|([^/]++)/settings(*:273)'
                    .')'
                .')'
                .'|/translate/sources/([^/]++)/([^/]++)/([^/]++)(*:328)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        189 => [[['_route' => 'app_profile', '_controller' => 'App\\Controller\\FrontendController::profil'], ['pseudo'], null, null, false, true, null]],
        213 => [[['_route' => 'app_project', '_controller' => 'App\\Controller\\FrontendController::project'], ['slugname'], null, null, false, true, null]],
        248 => [[['_route' => 'app_project_translate', '_controller' => 'App\\Controller\\FrontendController::projectTranslate'], ['slugname', 'language'], null, null, false, true, null]],
        273 => [[['_route' => 'app_project_settings', '_controller' => 'App\\Controller\\FrontendController::projectSettings'], ['slugname'], null, null, false, false, null]],
        328 => [
            [['_route' => 'app_source_translate', '_controller' => 'App\\Controller\\FrontendController::sourceTranslate'], ['slugname', 'idSource', 'language'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
